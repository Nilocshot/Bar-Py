#!/usr/bin/env python2
import os
import re
import sys
import subprocess
import threading
import Queue
import time

base00 = "#2B2B2b"
base01 = "#272935"
base02 = "#3A4055"
base03 = "#5A647E"
base04 = "#D4CFC9"
base05 = "#E6E1DC"
base06 = "#F4F1ED"
base07 = "#F9F7F3"
base08 = "#DA4939"
base0D = "#6D9CBE"

right_monitor = "DVI-I-1"
left_monitor = "DVI-D-0"
bspc_args = ["bspc", "control", "--get-status"]
lemonbar_args = ["lemonbar", "-p", "-f 'Hack:pixelsize=12'", "-gx20", "-B"+base00]
xtitle_args = ["xtitle", "-t 50"]
time_args = ["date", "+%F %H:%M:%S"]

def build(index, data):
    buff = ""
    if index == "O":
        buff=buff+"%{F"+base02+"}%{B"+base0D+"}"+" "+data+" "+"%{F-}%{B-}"
    elif index == "o":
        buff=buff+"%{F"+base0D+"}%{B"+base02+"}"+" "+data+" "+"%{F-}%{B-}"
    elif index == "F":
        buff=buff+"%{F"+base02+"}%{B"+base0D+"}"+" "+data+" "+"%{F-}%{B-}"
    elif (index == "u") or (index == "U"):
        buff=buff+"%{F"+base02+"}%{B"+base08+"}"+" "+data+" "+"%{F-}%{B-}"
    return buff

def parse(fifo):
    output_right = ""
    output_left = ""
    line = fifo.rstrip('\n')
    index = line[0]
    data = line[1:]
    if index == "W":
        output_left = output_left + "%{l}"
        output_right = output_right + "%{l}"
        for p in re.split('[mM]', data):
            if (p[0:7] == right_monitor):
                p = p[8:].rstrip(':')
                for i in re.split(':', p):
                    index = i[0]
                    data = i[1:]
                    output_right = output_right + build(index, data)
            elif (p[0:7] == left_monitor):
                p = p[8:].rstrip(':')
                for i in re.split(':', p):
                    index = i[0]
                    data = i[1:]
                    output_left = output_left +build(index, data)
    elif index == "X":
        output_left = "%{c}%{F"+base05+"}" + data + "%{F-}"
        output_right = output_left
    elif index == "T":
        output_left = "%{r}" + data
        output_right = output_left

    output="%{S0}"+output_left+"%{S1}"+output_right
    return output

def enqueue_out(out, queue):
    for line in iter(out.readline, b''):
        queue.put(line)
    out.close()

bar_proc = subprocess.Popen(lemonbar_args, stdin=subprocess.PIPE)

def bar_out():
    bar_proc.stdin.write(str(output + '\n'))

if __name__ == "__main__":
    while True:
        bspc_output = subprocess.check_output(bspc_args)
        xtitle_output = "X" + subprocess.check_output(xtitle_args)
        time_output = "T" + subprocess.check_output(time_args)
        output = parse(bspc_output) + parse(xtitle_output) + parse(time_output)
        bar_thread = threading.Thread(target=bar_out)
        bar_thread.start()
        time.sleep(0.1)
